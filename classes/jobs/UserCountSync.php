<?php

/**
 * Curse Inc.
 * AllSites
 * UserCountSync Class
 *
 * @author		Alexia E. Smith
 * @copyright	(c) 2015 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		AllSites
 * @link		https://gitlab.com/hydrawiki
 *
 **/

namespace AllSites\Jobs;

use Exception;
use RedisCache;
use SyncService\Job;
use AllSites\UserCount;
use DynamicSettings\Wiki;

class UserCountSync extends Job {
	/**
	 * Triggers user counts against a specific wiki.
	 *
	 * @access	public
	 * @param	array	$args Named arguments passed by the command that queued this job.
	 *                    - site_key	string	The MD5 site key for the wiki.
	 * @return	integer	Exit value for this thread.
	 */
	public function execute($args = []) {
		$redis = RedisCache::getClient('cache');
		$wiki = isset($args['site_key']) ? Wiki::loadFromHash($args['site_key']) : false;

		if (!$wiki) {
			$this->outputLine(__METHOD__ . ": Wiki site not found.", time());
			return 1;
		}

		$dbKey = $wiki->getSiteKey();

		$this->dbs[$dbKey] = $wiki->getDatabaseLB();

		try {
			$db = $this->dbs[$dbKey]->getConnection(DB_MASTER);

			$userCounter = new UserCount($db, $redis);
			$userCounter->pushUniqueUsers();
		} catch (Exception $e) {
			$this->outputLine(__METHOD__ . " - Unable to connect to database: " . $e->getMessage(), time());
			return 1;
		}
		return 0;
	}
}
